/*
 * Gcm sdk for google chrome extensions
 */

function GcmSDK(options) {
    this.senderIds = options["senderIds"];
    this.registrationId = ""
}

GcmSDK.prototype.register = function(callback) {
    chrome.gcm.register(this.senderIds, function(registrationId) {
        console.log(registrationId);
        this.registrationId = registrationId;
        callback.call(null, registrationId);
    });

    chrome.gcm.onMessage.addListener(function(message) {
        this.showNotification("How cool is that!", message.toString());
    });
}

GcmSDK.prototype.generateNotificationId = function() {
    var id = Math.floor(Math.random() * 9007199254740992) + 1;
    return id.toString();
}

GcmSDK.prototype.showNotification = function(title, message) {
    var notificationId = this.gnerateNotificationId();
    var notificationOptions = {
        title: title,
        message: message,
        type: "basic",
        iconUrl: "notification_icon.jpeg"
    };
    chrome.notifications.create(notificationId, notificationOptions, function() {});
}

GcmSDK.prototype.generateCURLCommand = function(options) {
    return command = 'curl' +
      ' -H "Content-Type:application/x-www-form-urlencoded;charset=UTF-8"' +
      ' -H "Authorization: key=' + options["apiKey"] + '"' +
      ' -d "registration_id=' + this.registrationId + '"' +
      ' -d data.' + "TestMessageKey" + '=' + "TestMessageValue" +
      ' https://android.googleapis.com/gcm/send';
}

