/*
 * Constructor
 */
function CsfSDK(options) {
  this.clientId = options["clientId"];
  this.gcmRegistrationId = options["gcmRegistrationId"];
  this.host = "https://login.salesforce.com";
  this.callbackUri = chrome.identity.getRedirectURL() + "provider_cb";
  this.userInfo = null;
}

/*
 * Initialize authentication flow
 */
CsfSDK.prototype.authenticate = function(callback) {
    var options = {
      "interactive": true,
      "url": this.host + "/services/oauth2/authorize" +
                "?client_id=" + this.clientId +
                "&response_type=token" +
                "&display=login" +
                "&scope=api" +
                "&redirect_uri=" + encodeURIComponent(this.callbackUri)
    };

    console.log("Starting web auth flow");
    // bind CsfSDK context instead of launchWebAuth this
    chrome.identity.launchWebAuthFlow(options, this.handleAuthenticationResponse.bind(this, callback));
}

/* 
 * Chrome identity authentication flow callback.
 * On success we are fetching user's ID data and registering this extension for push notifications
 */
CsfSDK.prototype.handleAuthenticationResponse = function(callback, redirectUri) {
  console.log("Handle authentication response", redirectUri);

  this.extractAuthenticationResponseData(redirectUri);
  
  this.fetchIdData().then(
    function(data) {
      callback.call(this, data);
    },
    function(error) {
      callback.call(this, undefined);
    }
  )
}

/*
 * Extract accessToken, instanceUrl, idUri from successful authentication response redirectUri
 */
CsfSDK.prototype.extractAuthenticationResponseData = function(redirectUri) {
    console.log("Extracting authentication response data", redirectUri);

    var redirectRe = new RegExp(this.callbackUri + '[#\?](.*)');
    var matches = redirectUri.match(redirectRe);
    var parts = this.parseRedirectFragment(matches[1]);

    this.accessToken = parts["access_token"];
    this.instanceUrl = decodeURIComponent(parts["instance_url"]);
    this.idUrl = decodeURIComponent(parts["id"]);
}

/*
 * Fetch logged user ID data from idIUrl recieved on successful authentication flow.
 * return Promise
 *    resolve(userDataObject)
 *    reject(withErrorMessage)
 */
CsfSDK.prototype.fetchIdData = function() {
  console.log("Fetching ID data");

  var that = this;

  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", that.idUrl + "?oauth_token=" + that.accessToken, true);
    xhr.responseType = "json";

    xhr.onload = function() {
      if (this.status < 200 || this.status >=300) {
        console.log("reject promise");
        reject(xhr.statusText);
      } else {
        console.log("resolve promise");
        resolve(this.response);
      }
    }
    xhr.send();
  });
}

/*
 * Register extension as salesforce external app for google push notifications using prefetched
 * Google Cloud Messaging Registration Id.
 */
CsfSDK.prototype.registerPushServiceDevice = function(gcmRegistrationId) {
  console.log("Registering push service device");

  var that = this;

  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", instanceUrl + "/services/data/v31.0/sobjects/MobilePushServiceDevice", true);
    xhr.responseType = "json";
    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onload = function() {
      if (this.status < 200 || this.status >=300) {
        console.log("reject promise");
        reject(xhr.statusText);
      } else {
        console.log("resolve promise");
        resolve(this.response);
      }
    }
    xhr.send(JSON.stringify({"ConnectionToken": gcmRegistrationId}));
  });
}

/*
 * Convinience method for extracting query params from recieved fragment
 */
CsfSDK.prototype.parseRedirectFragment = function(fragment) {
    var pairs = fragment.split(/&/);
    var values = {};

    pairs.forEach(function(pair) {
        var nameVal = pair.split(/=/);
        values[nameVal[0]] = nameVal[1];
    });

    return values;
}
