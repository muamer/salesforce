// Initialize extension
function initExtension() {
    chrome.storage.local.get("gcmRegistrationId", function(result) {
        // Already registered
        if(result["gcmRegistrationId"]) return;

        // Register extension to gcm
        var senderIds = ["3282324176"];
        var gcmSdk = new GcmSDK({
            "senderIds": senderIds
        });
        gcmSdk.register(function(registrationId) {
            chrome.storage.local.set({"gcmRegistrationId": registrationId});
        });
    });
};

chrome.runtime.onInstalled.addListener(initExtension);
chrome.runtime.onStartup.addListener(initExtension);
